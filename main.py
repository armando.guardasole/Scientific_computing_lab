import numpy as np
import graphviz as gv
from dijkstraKSP import dijkstraK
from KSPDTM import KSPDTM

#Example of usage of the KSPDTM and dijkstraK functions

#Get the V vector

ind = 2 #Use long names
#ind = 1 #Use short names
#ind = 0 #Use indexes
V = np.loadtxt('Stop-Bahn.giv',delimiter=';',usecols=ind,dtype=str)
m=len(V)

#Get the vector of edges' ids
edges = np.loadtxt('Edge-Bahn.giv',delimiter=';',usecols=(1,2),
                   dtype=int).tolist()

#From the edges ids, get the E vector
E = [[V[e[0]-1],V[e[1]-1]]  for e in edges ]

n=len(E)

#Choice of weights

#ind = 3 #Length
#ind = 4 #Min-time
ind = 5 #Max-time

W = np.loadtxt('Edge-Bahn.giv',delimiter=';',usecols=ind)

#Build the graph as dictionary of dictionaries
G = {}
for v in V:
    G[v] = {}
for i in range(n):
    G[E[i][0]][E[i][1]] = G[E[i][1]][E[i][0]] = W[i]
    

#Graph plot with graphviz
drawG = gv.Graph(format='png',graph_attr={'ratio':'1'})
for e in range(n):
    drawG.edge(E[e][0],E[e][1],label=str(W[e]),weight=str(W[e]))
    
#Get the two shortest paths between a pair of nodes u and v
    
u = V[0]
v = V[5]

P = dijkstraK(u,2,G)
P = [p[1] for p in P if p[1][-1]==v]
PE = [[[p[r],p[r+1]] for r in range(len(p)-1)] for p in P]

#Graph plot of shortest and 2nd-shortest path from u to v

drawG1 = gv.Graph(format='png',graph_attr={'ratio':'1'})
drawG2 = gv.Graph(format='png',graph_attr={'ratio':'1'})

for e in range(n):
    if E[e] in PE[0] or (E[e][::-1] in E and E[e][::-1] in PE[0]):
        drawG1.edge(E[e][0],E[e][1],label=str(W[e]),weight=str(W[e]),
                    color='red')
    else:
        drawG1.edge(E[e][0],E[e][1],label=str(W[e]),weight=str(W[e]))
        
for e in range(n):
    if E[e] in PE[1] or (E[e][::-1] in E and E[e][::-1] in PE[1]):
        drawG2.edge(E[e][0],E[e][1],label=str(W[e]),weight=str(W[e]),
                    color='green')
    else:
        drawG2.edge(E[e][0],E[e][1],label=str(W[e]),weight=str(W[e]))

drawG.render(filename='graph')
drawG1.render(filename='shortest_path')
drawG2.render(filename='2nd-shortest_path')


#Get the L list
H = np.loadtxt('Pool-bahn.giv',delimiter=';',usecols=(0,2),dtype=int)
l=len(H)
h = H[-1][0]
L=[[ E[H[r][1]-1] for r in range(l) if H[r][0]==j+1] for j in range(h)]

#Get the tr matrix
tr = np.loadtxt('OD-bahn.giv',delimiter=';',usecols=2).reshape([m,m])

#Get the f_min and f_max vectors
f_min = np.loadtxt('Load-bahn.giv',delimiter=';',usecols=2)
f_max = np.loadtxt('Load-bahn.giv',delimiter=';',usecols=3)

#Optimize line frequencies with k = 4, maximum capacity C = 500

OPT = KSPDTM(G,4,tr,500,L,V,E,f_min,f_max)

#Get the status of the optimization

status = OPT[-1]

if status != 1: #Optimization failed
    print('Optimization failed. Error '+str(status)+'\n')
else: #Optimization ended successfully    
    #Optimized frequencies
    O = len(OPT)
    for i in range(O-2):
        print('f('+str(i)+') = '+str(OPT[i])+'\n')
    #Get percentage of direct travellers:
    print('Direct travellers percentage = '+str((OPT[-2]/tr.sum())*100)+'%')









