import heapq as hq  
def dijkstra(s,G):    
    ''' Dijkistra algorithm for shortest path search in a positively
    weighted graph.
    Parameters: s(type of elements of G) Source node
                G(dictionary) Graph 
    Returns: T (dictionary) shortest path tree from s 
    '''
    
    
#    The graph G is represented as a dictionary of dictionaries:
#     the keys v_j j=1,..n are the nodes of the graph, and the value of each
#     v_j is a dictionary where every key is an out-neighbour of v_j, and its
#     value is the weight on the out-link. Thanks to this representation
#     the nodes do not have to be given as integers from 1 to |V|.
#     An undirected graph is simply represented by with a dictionary where
#     every node is also a key in the dictionary of its neighbours (and the
#     edges obviously have the same weight).
    
    
    inf = float('inf') #We initialize the length of the path from the source
                       #to any unvisited node to +infinity.
    
    prev = {}          #For every v in V, prev(v) is the node that precedes
                       #v in a shortest path from the source s to v
    
    d = {}             #d(v) length of the shortest path from
                       #the source to v
    
    PQ = []            #Priority queue

    for i in G: 
        d[i] = inf
        prev[i] = 0    #For every unvisited node, we initialize prev(v) as
                       #a non-existing node
    d[s] = 0
    
    
#    Here the priority queue is initialized by pushing all the nodes. 
#    Every element contains the node and the distance, which works as
#    a reverse priority. If two elements have the same priority, the order
#    in which they entered the queue is not important
#    The structure implemented in the heapq library works already as a
#    min-heap
    for i in G:
        hq.heappush(PQ,[d[i],i])
    
    while(PQ):
        x = hq.heappop(PQ) #Getting the element with minimum distance from s
        u = x[1] #The second element of the list is the node
        
        if ( u == inf ): #No shortest path exist between s and any other
            break        #node that is still in Q
        
#        Here we loop through all the neighbours of the selected 
#        min-priority node that have not been visited yet. If a shortest
#        path is found, the distance is updated
        
        for v in G[u]:                       
            if ( v in [i[1] for i in PQ]):
                d_new = d[u] + G[u][v]
                if d_new < d[v]:
                    #The queue built with heapq does not allow updating,
                    #so this step slows the performance
                    PQ[PQ.index([d[v],v])][0] = d_new
                    d[v] = d_new                 
                    prev[v] = u
                    
#    By looping on the prev list, we obtain the shortest path tree as
#    a subgraph of G
    
    
    T = {}  #If the given node is isolated or it does not exist,
            #the function returns an empty dictionary
    for i in prev:
        if prev[i] != 0:
            if not prev[i] in T:
                T[prev[i]] = {}
            T[prev[i]][i] = G[prev[i]][i]
    return T


