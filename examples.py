'''Examples G1 and G2 were obtained from https://toreopsahl.com/datasets
.This script reads from file and converts into the 
supported format'''

'''Small example from the article by Aljazzar and Leue
|V| = 7, |E| = 15 (mainly to test for correctness)'''
G0 = {'s0':{'s1':3,'s2':5,},'s1':{'s2':4,'s6':7},
      's2':{'s0':2,'s1':1,'s3':16,'s4':1},'s3':{'s5':3},
      's4':{'s1':1,'s2':1,'s3':7,'s6':1},'s5':{'s6':4}}

'''|V| = 500, |E| = 5960'''
F = open('USairport500.txt')
g = F.read()
g = g.split('\n')
g.sort()
g=' '.join(g)
g=g.split(' ')
kk=[]
for i in range(0,len(g)):
    if g[i]:
        kk.append(g[i])
g=kk
i=0
j=1
k=2
G1 = {}
while i < len(g):
    v = g[i]
    G1[v] = {}
    
    while (g[i] == v):
        if not g[j] in G1:
            G1[g[j]] = {}
        G1[v][g[j]] = float(g[k])
        G1[g[j]][v] = float(g[k])
        i=i+3
        k=k+3
        j=j+3
        if ( i >= len(g)):
            break


'''|V| = 7976, |E| = 30501'''
F = open('openflights.txt')
g = F.read()
g = g.split('\n')
g.sort()
g=' '.join(g)
g=g.split(' ')
kk=[]
for i in range(0,len(g)):
    if g[i]:
        kk.append(g[i])

g=kk
i=0
j=1
k=2
G2 = {}

while i < len(g):
    v = g[i]
    G2[v] = {}
    while (g[i] == v):
        G2[v][g[j]] = float(g[k])
        i=i+3
        k=k+3
        j=j+3
        if ( i >= len(g)):
            break
    