import heapq as hq

def dijkstraK(s,k,G):
    '''Dijkstra algorithm for the k-shortest path (Loops not allowed).
    
    Parameters: s(type of elements of G) Source node
                k (int) Number of shortest paths to compute
                G(dictionary) Graph 
    
    Returns: SK (list) each element of the list is a list with two element;
             the second element is a path from s to another node in G. The 
             first element is th length of the path, represented is a list
             of its nodes.
    '''


    SK = [] #List cointaing the k shortest path from s to every other node
            #in G
   
#    In the Dijkstra algorithm for the SP problem we used a priority
#    queue which stored all the nodes that were not visited yet. Here
#    instead every node is visited exactly k times
    
    count = {} #Counts how many times a node has been visited 
    for u in G: #Initialized to zero
        count[u] = 0
    H = [[0,[s]]] #Initialize the heap with only the trivial path
                  #from s to s
    
#    Here again we use the reverse priority d[u] = length of the shortest
#    path from s to u. We store the entire path in the heap, but actually only
#    the last node is used.
                  
    while H:
        P_u = hq.heappop(H) #count[u]-th shortest path from s to u
        d = P_u[0]
        u = P_u[1][-1]
        if not u in G: #If the graph has nodes with no outlinks which
            G[u] = {}  #do not appear as keys in G, we define their attribute
            count[u] = 0 #as an empty dictionary
        
        count[u]=count[u]+1
            
        if count[u] <= k:
            if (u!=s): #No output of the trivial path 
                SK.append(P_u)
            for v in G[u]: #Visit the neighbours of u and insert all the new
                           #paths
                if not v in P_u[1]:
                    P_v = [d + G[u][v], P_u[1]+[v]]
                    hq.heappush(H,P_v)
    return SK
