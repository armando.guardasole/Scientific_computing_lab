import pulp as pl
import numpy as np
from dijkstraKSP import dijkstraK

def KSPDTM(G,k,tr,C,L,V,E,f_min=None,f_max=None):
    '''Direct travelers model adapted to k-shortest path. The problem is
    formulated as mixed-integer linear programming using the PuLP package

    Parameters:
    G: weighted directed graph represented as dictionary of dictionaries
    k: number of k-shortest path to consider
    tr: traffic demand matrix
    C: fixed vehicle capacity
    L: set of all considered lines (each line is a list of edges id's)
    V: array of nodes labels 
    E: array of edges
    f_min,f_max: arrays of minimum and maximum frequency allowed 
    on each edge. If f_max is not given, it is set to infinity (that is,
    we do not apply an upper bound constraint).
    If f_max is not given either, we set f_min = f_max = lfr where lfr is
    the edge frequency load estimated from the matrix tr
    
    Returns:
        A |L|+2-dimensional vector - the first |L| components are the 
        chosen frequencies for each line. The last component is an integer
        representing the status of the optimization (1 = Success),
        and the 2nd-to-last is the value of the objective function
'''



    
    l = len(L)
    n = len(E)
    m = len(V)
    set_min = 1
    
    if f_min is None:
        set_min = 0
        lfr = np.zeros(n)
    elif f_max is None:
        set_max = 0
    else:
        set_max = 1

    
    
    #Initialize the formulation of the problem
    MIP =  pl.LpProblem("KSPDTM",pl.LpMaximize)
    
    VV = [(V[u],V[v]) for u in range(m) for v in range(m)]
    
    #Initialize the variables D_ij for i,j in V as positive and continuous
    #representing the direct travellers
    D_vars = pl.LpVariable.dicts('D',VV,
    lowBound=0)
    
    #Initialize the variables f_j for j in 0,...,|L|-1 as positive and 
    #integer, representing the lines frequencies
    f_vars = pl.LpVariable.dicts('f',[j for j in range(m)],
    lowBound=0,cat='Integer')
    
    #Define the objective function to maximize as the sum of the D_ij's
    MIP += pl.lpSum([D_vars[i] for i in VV])
    
    #Number of direct travellers between u and v must be smaller
    #than the total number of travellers
    for u in range(m):
        for v in range(m):
            MIP += D_vars[(V[u],V[v])] <= tr[u][v]

    #Constraint to avoid line overloading, assuming that that each 
    #passengers chooses a line to go to u to v only if there is at least
    #one k-shortest path from u to v in that line
    for u in range(m):    
        P = dijkstraK(V[u],k,G) #Get the k-shortest paths from u to any other
                            #node
        P = [p[1] for p in P]
        for v in range(m):
            if u == v:     #No constraint on the variables D_ii
                continue
            KP = [p for p in P if p[-1]==V[v]]
                            #Get the k-shortest paths from u to v
            if KP == []:    
                continue
                             
                             #And rebuild each path as a set of edges
            PE = [[[p[r],p[r+1]] for r in range(len(p)-1)]
            for p in KP]
           
            
#            For every line, check if there is any k-shortest path 
#            between u and v included in the line. If so, add the line to
#            the constraint
            line_bool = np.ones(l)
            r=0
            for le in L:
                for pe in PE:
                    included=1
                    for e in pe:
                        #Also check for reversed edge if lines are given
                        #as edges of an undirected graph
                        if not (e in le or (e[::-1] in E and e[::-1] in le)):
                        
                            included=0
                            break
                    if included:
                        break
                if included==0:
                    line_bool[r] = 0
                r=r+1
#                
            MIP += D_vars[(V[u],V[v])] - C*pl.lpSum([f_vars[r]
            for r in range(l) if line_bool[r]]) <= 0
            
            #If f_min and f_max are not defined, estimate lfr
            if set_min == 0:
                for r in range(n):
                    for pe in PE:
                        if E[r] in pe:
                            lfr[r] = lfr[r] + tr[u][v]
                            break

  
    #Constraint on min and max frequency. For every edge, select all the
    #lines containing the edge
    edge_line_bool = np.zeros([n,l])
    for s in range(n):
        r = 0
        for le in L:
            if E[s] in le:
                edge_line_bool[s][r] = 1
            r=r+1
    
    #The constraints are added to the model. Again if f_min and f_max
    #are not given, we use lfr
    if set_min == 0:
        for s in range(n):
            MIP += pl.lpSum([f_vars[r]
            for r in range(l) if edge_line_bool[s][r]]) == lfr[s]//C
    else:
        for s in range(n):
            if set_max == 1:
                MIP += pl.lpSum([f_vars[r]
                for r in range(l) if edge_line_bool[s][r]]) <= f_max[s]
            MIP += pl.lpSum([f_vars[r]
            for r in range(l) if edge_line_bool[s][r]]) >= f_min[s]

    status = MIP.solve()
    
    return [pl.value(f_vars[i]) for i in range(l)]+\
            [pl.value(MIP.objective),status]

            
