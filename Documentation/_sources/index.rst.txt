Documentation for the K-shortest path direct travellers model
*************************************************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Introduction
   dijkstraK
   KSPDTM

.. only:: html 

   ******************
   Indices and Tables
   ******************

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

